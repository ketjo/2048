# 2048

Based on [2048](http://saming.fr/p/2048/).

[You can play it here!](https://ketjo.gitlab.io/2048/)

### Contributions

[Erlan](https://gitlab.com/users/zharkeev.post/projects)

### Main screen

![App main screen](./src/assets/img/2048.png 'main screen')

## Contributing

Changes and improvements are more than welcome! Feel free to fork and open a pull request. Please make your changes in a specific branch and request to pull into `dev`! If you can, please make sure the game fully works before sending the PR, as that will help speed up the process.

### Recommendation
Node v14.20.1
Package manager `yarn`